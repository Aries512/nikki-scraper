import scraper

# scrape specific category/categories by specifying them in an array
# list of all categories is in scraper.py
scraper.create_sheet(scraper.categories_objects(["accessory"]), "category_objects")
