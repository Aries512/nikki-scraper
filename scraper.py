import requests
from bs4 import BeautifulSoup
import xlwt
import emoji
import re


NAME = "Name"
ID = "ID"
HEARTS = "Rarity"
ATTRIBUTE1 = "Attribute 1"
ATTRIBUTE2 = "Attribute 2"
SUIT_NAME = "Suit"
SUIT_STYLE = "Style"
SUBCATEGORY1 = "Type 1"
SUBCATEGORY2 = "Type 2"
COLUMNS = [ID, NAME, SUIT_NAME, HEARTS, SUIT_STYLE, SUBCATEGORY1, SUBCATEGORY2, ATTRIBUTE1, ATTRIBUTE2]

CATEGORIES = ["hair", "dress", "coat", "top", "bottom", "hosiery", "shoes", "makeup", "accessory", "soul"]

SITE_URL = "https://ln.nikkis.info"
WARDROBE_URL = "https://ln.nikkis.info/wardrobe/"
NEW_OBJECTS_URL = "https://ln.nikkis.info/new/"


def soup_object(url):
    print(url)
    response = requests.get(url)
    return BeautifulSoup(response.text, "html.parser")

def content(soup, selector, elemId = 0, contentId = 0):
    try:
        return soup.select(selector)[elemId].contents[contentId]
    except IndexError:
        return ''

def section(soup, section_title):
    return next((s for s in soup.select(".section") if content(s, "h5") == section_title), None)

def parse_object(soup, category):
    hearts_section = section(soup, "Rarity")
    hearts_cnt = int(content(hearts_section, ".center > p span", 1))
    is_heart_yellow = len(hearts_section.select(".yellow-text")) >  0
    suit_section = section(soup, "Suit")
    subcategory_search = re.search("\((.+?)\)", content(soup, ".container > div.collection > div > p", 0, 2))
    subcategory = subcategory_search.group(1) if subcategory_search else ''
    subcategory1 = '' if category == "hosiery" else subcategory
    subcategory2 = subcategory if category == "hosiery" else ''
    return {
        NAME: content(soup, ".header"),
        ID: int(content(soup, ".collection-item > p > strong").split()[-1]),
        HEARTS:  hearts_cnt * (emoji.emojize(':yellow_heart:') if is_heart_yellow else u'\u2764\uFE0F'),
        ATTRIBUTE1: content(soup, '.special-attr', 0),
        ATTRIBUTE2: content(soup, '.special-attr', 1),
        SUIT_NAME: content(suit_section, "div > div > a") if suit_section else '',
        SUIT_STYLE: content(suit_section, "div > div > p > strong") if suit_section else '',
        SUBCATEGORY1: subcategory1,
        SUBCATEGORY2: subcategory2,
        }

def category_objects(category):
    soup = soup_object(WARDROBE_URL + category)
    category_object_urls = [SITE_URL + e["href"] for e in soup.select(".collection > a")]
    return [parse_object(soup_object(url), category) for url in category_object_urls]

def categories_objects(categories = CATEGORIES):
    return {category: category_objects(category) for category in categories}

def new_objects():
    soup = soup_object(NEW_OBJECTS_URL)
    section = next((li for li in soup.select("li") if content(li, "strong") == "Clothes"), None)
    urls = [SITE_URL + a["href"] for a in section.select("a") if a["href"].startswith("/wardrobe/")]
    category_objects = {category: [] for category in CATEGORIES}
    for url in urls:
        category = re.search("wardrobe/(.+?)/", url).group(1)
        category_objects[category].append(parse_object(soup_object(url), category))
    return category_objects

def create_sheet(objects_by_category, file_name):
    book = xlwt.Workbook()
    for category, objects in objects_by_category.items():
        sheet = book.add_sheet(category.capitalize())
        for idx, col in enumerate(COLUMNS):
            sheet.write(0, idx, col)
        for idx, object in enumerate(objects):
            for key, val in object.items():
                sheet.write(idx + 1, COLUMNS.index(key), val)
    book.save(file_name + '.xls')
